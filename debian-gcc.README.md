# Docker Debian gcc

### Build the image 

```sh
$ docker build -t ${NAME_OF_YOUR_CHOICE} . 
```

If you Dockerfile have a different file name :
```sh
$ docker build -t ${NAME_OF_YOUR_CHOICE} -f ${YOUR_FILE_NAME} . 
```
### Run the image

```sh
$ docker run -ti -v ${MOUNT_VOLUME_FROM_YOUR_LS}:/docker-pds ${NAME_OF_YOUR_CHOICE}
```

### Add an alias

Open your ~/.bashrc or ~/.zshrc

```

# alias for docker

alias ${NAME_COMMAND} = "docker run -ti -v ${MOUNT_VOLUME_FROM_YOUR_LS}:/docker-pds ${NAME_OF_YOUR_CHOICE}"
```
